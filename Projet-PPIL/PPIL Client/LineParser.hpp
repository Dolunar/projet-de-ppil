#pragma once
#include "ShapeParserChain.hpp"
#include "Line.hpp"

using namespace std;

class LineParser : public ChainShapeParser {
public:
    LineParser(IShapeParser* nextParser = nullptr) : ChainShapeParser(nextParser) 
    {
    }

protected:
    virtual IShape* doParse(list<string>& lines) const {
        double x1, y1, x2, y2;
        unsigned long colorCode;
        string line;
        if (sscanf(lines.front().c_str(), "LINE %lf %lf %lf %lf %lx", &x1, &y1, &x2, &y2, &colorCode) != 5)
        {
            return nullptr;
        }
        lines.pop_front();
        return new Line(Vector2d(x1, y1), Vector2d(x2, y2), colorCode);
    }
};