#pragma once
#include <iostream>
#include <vector>
#include "IShape.hpp"

using std::vector;
using std::invalid_argument;
using std::abs;
using std::ostringstream;

class Polygone : public IShape
{
private:
    vector<Vector2d> vertices;
    unsigned int colorCode;

public:
    Polygone(const vector<Vector2d>& vertices, unsigned int colorCode)
        : vertices(vertices), colorCode(colorCode) {
        if (vertices.size() < 3) {
            throw invalid_argument("Polygon must have at least 3 vertices");
        }
    }

    Polygone(std::initializer_list<Vector2d> initList, unsigned int colorCode)
        : Polygone(vector<Vector2d>(initList), colorCode) {}

    const vector<Vector2d>& GetVertices() const {
        return vertices;
    }

    unsigned int GetColorCode() const {
        return colorCode;
    }

    virtual double GetArea() const override {
        double area = 0;

        for (int i = 0; i < vertices.size(); i++) 
        {
            Vector2d a = vertices[i];
            Vector2d b;

            if (i != vertices.size() - 1) 
            {
                b = vertices[i + 1];
            }
            else 
            {
                b = vertices[0];
            }

            area += (b.x - a.x) * (b.y + a.y);
        }

        area = abs(area);
        return 0.5 * area;
    }

    virtual operator string() const override {
        ostringstream os;
        os << "POLYGON " << vertices.size() << " " << hex << colorCode << "\n";
        for (const Vector2d& vertex : vertices) {
            os << vertex << "\n";
        }
        return os.str();
    }

    virtual Polygone* Clone() const override {
        return new Polygone(vertices, colorCode);
    }

    virtual void Accept(IVisitor& visitor) const override {
        visitor.Visit(*this);
    }

    virtual Polygone* Accept(const ITransformationVisitor& transformationVisitor) const override {
        return transformationVisitor.Visit(*this);
    }
};