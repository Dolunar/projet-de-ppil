#pragma once
#include <iostream>

using namespace std;

class ISocket
{
public:
	virtual void Open() = 0;
	virtual void Send(string msg) = 0;
	virtual void Close() = 0;
};