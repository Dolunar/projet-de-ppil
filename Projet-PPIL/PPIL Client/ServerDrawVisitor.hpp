#pragma once
#include <iostream>
#include <vector>
#include <sstream>
#include "IVisitor.hpp"
#include "ISocket.hpp"
#include "Circle.hpp"
#include "Line.hpp"
#include "Polygon.hpp"

using namespace std;

const string END_MESSAGE = "END";

class ServerDrawVisitor : public IVisitor
{
private:
	ISocket* socket;

public:
	ServerDrawVisitor(ISocket* socket)
	{
		if (socket == nullptr)
		{
			throw invalid_argument("Socket must not be null");
		}
		this->socket = socket;
	}

	virtual void Visit(const Circle& circle)
	{
		SendToServer<const Circle&>(circle);
	}

	virtual void Visit(const Line& line)
	{
		SendToServer<const Line&>(line);
	}

	virtual void Visit(const Polygone& polygon)
	{
		SendToServer<const Polygone&>(polygon);
	}

	virtual void Visit(const ShapeGroup& shapeGroup)
	{
		SendToServer<const ShapeGroup&>(shapeGroup);
	}

private:
	const vector<string> SplitLines(const string& str) {
		vector<string> lines;
		string line;
		istringstream iss(str);

		while (getline(iss, line))
		{
			lines.push_back(line);
		}

		return lines;
	}

	template <typename T>
	void SendToServer(T shape) {
		for (const string& line : SplitLines((string)shape))
		{
			socket->Send(line);
		}
		socket->Send(END_MESSAGE);
	}

};