#pragma once
#include <iostream>
#include <list>
#include "IShape.hpp"

using namespace std;

class IShapeParser
{
public:
	virtual IShape* parse(list<string>& input) const = 0;
};