#pragma once
#include <iostream>

class Vector2d;
class Circle;
class Line;
class Polygone;
class ShapeGroup;

class ITransformationVisitor
{
public:
	virtual Vector2d Visit(const Vector2d& vector2d) const = 0;
	virtual Circle* Visit(const Circle& circle) const = 0;
	virtual Line* Visit(const Line& line) const = 0;
	virtual Polygone* Visit(const Polygone& polygon) const = 0;
	virtual ShapeGroup* Visit(const ShapeGroup& shapeGroup) const = 0;
};