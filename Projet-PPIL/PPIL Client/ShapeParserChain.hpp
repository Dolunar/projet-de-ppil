#pragma once
#include <fstream>
#include "IShapeParser.hpp"

using namespace std;

class ChainShapeParser : public IShapeParser {
private:
    IShapeParser* nextParser;

protected:
    // La m�thode doParse est maintenant abstraite, elle doit �tre impl�ment�e par les classes d�riv�es.
    virtual IShape* doParse(list<string>& input) const = 0;

public:
    // Utilisation de nullptr au lieu de NULL pour la conformit� avec le C++ moderne.
    explicit ChainShapeParser(IShapeParser* nextParser = nullptr)
        : nextParser(nextParser) {}

    // Impl�mentation de la m�thode parse d�finie dans l'interface IShapeParser.
    virtual IShape* parse(list<string>& input) const override {
        if (input.empty()) {
            return nullptr;
        }

        IShape* output = doParse(input);
        if (output != nullptr) {
            return output;
        }
        else if (nextParser != nullptr) {
            return nextParser->parse(input);
        }

        return nullptr;
    }
};