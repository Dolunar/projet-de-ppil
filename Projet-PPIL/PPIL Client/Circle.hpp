#pragma once
#include <iostream>
#include "IShape.hpp"
#define PI 3.14

using namespace std;

class Circle : public IShape 
{
private:
	Vector2d center;
	double radius;
	unsigned int colorCode;

public:
	Circle(const Vector2d& center, const double& radius, const unsigned int& colorCode)
	{
		if (radius <= 0)
		{
			throw invalid_argument("Radius must be > 0");
		}
		this->center = center;
		this->radius = radius;
		this->colorCode = colorCode;
	}

	const Vector2d& GetCenter() const 
	{
		return center;
	}

	const double& GetRadius() const 
	{
		return radius;
	}

	unsigned int GetColorCode() const
	{
		return colorCode;
	}

	virtual double GetArea() const {
		return PI * radius * radius;
	}

	virtual operator string() const {
		ostringstream os;
		os << "CIRCLE " << center << " " << radius << " " << hex << colorCode << endl;
		return os.str();
	}

	virtual Circle* Clone() const
	{
		return new Circle(center, radius, colorCode);
	}

	virtual void Accept(IVisitor& visitor) const
	{
		visitor.Visit(*this);
	}

	virtual Circle* Accept(const ITransformationVisitor& transformationVisitor) const
	{
		return transformationVisitor.Visit(*this);
	}
};