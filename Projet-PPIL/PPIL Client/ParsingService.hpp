#pragma once
#include <fstream>
#include <list>
#include "IShape.hpp"
#include "IShapeParser.hpp"

using namespace std;

/*
class ParsingService
{
    IShapeParser* shapeParser;

    ParsingService();

public:
    static ParsingService& Instance() {
        static ParsingService instance;
        return instance;
    }

    void SetParser(IShapeParser* shapeParser) {
        this->shapeParser = shapeParser;
    }

    IShape* parse(const string& fileName)
    {
        if (shapeParser == nullptr)
        {
            throw runtime_error("ShapeParser is not initialized");
        }

        fstream file(fileName, ios::in);

        if (not file.is_open()) {
            return nullptr;
        }

        list<string> lines;
        string line;

        while (getline(file, line))
        {
            lines.push_back(line);
        }

        int startLine = 0;
        return shapeParser->parse(lines);
    }

    IShape* parse(std::list<std::string>& lines) {
        return shapeParser->parse(lines);
    }
};

*/

class ParsingService
{
public:
	static IShapeParser* shapeParser;
	ParsingService() = delete;

    static IShape* parse(const string& fileName) 
    {
        if (shapeParser == nullptr)
        {
            throw runtime_error("ShapeParser is not initialized");
        }

        fstream file(fileName, ios::in);

        if (not file.is_open()) {
            return nullptr;
        }

        list<string> lines;
        string line;

        while (getline(file, line))
        {
            lines.push_back(line);
        }

        int startLine = 0;
        return shapeParser->parse(lines);
    }

    static IShape* parse(std::list<std::string>& lines) {
        return shapeParser->parse(lines);
    }
};