#pragma once
#include <iostream>
#include "ITransformationVisitor.hpp"
#include "TransformationUtils.hpp"
#include "Vector2d.hpp"
#include "Circle.hpp"
#include "Line.hpp"
#include "Polygon.hpp"

class TranslationVisitor : public ITransformationVisitor {
private:
	const Vector2d movement;

public:
	TranslationVisitor(const Vector2d& movement) : movement(movement) {
	}

	virtual Vector2d Visit(const Vector2d& vector2d) const 
	{
		return vector2d + movement;
	}

	virtual Circle* Visit(const Circle& circle) const
	{
		return TransformationUtils::Visit(*this, circle);
	}

	virtual Line* Visit(const Line& line) const
	{
		return TransformationUtils::Visit(*this, line);
	}

	virtual Polygone* Visit(const Polygone& polygon) const
	{
		return TransformationUtils::Visit(*this, polygon);
	}

	virtual ShapeGroup* Visit(const ShapeGroup& shapeGroup) const
	{
		return TransformationUtils::Visit(*this, shapeGroup);
	}
};

