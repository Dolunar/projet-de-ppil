#pragma once
#include <iostream>
#include <cmath>
#include <sstream>
#include "ITransformationVisitor.hpp"

using namespace std;

template <class T>
inline const T operator - (const T& u, const T& v) 
{
    return u + (-v);
}

class Vector2d
{
public:
    double x;
    double y;

    explicit Vector2d(const double& x = 0, const double& y = 0) : x(x), y(y) 
    {
    }

    Vector2d(const Vector2d& u) : Vector2d(u.x, u.y) 
    {
    }

    const Vector2d operator + (const Vector2d& u) const 
    {
        return Vector2d(x + u.x, y + u.y);
    }

    const Vector2d operator * (const double& a) const 
    {
        return Vector2d(x * a, y * a);
    }

    friend const Vector2d operator * (const double& a, const Vector2d& u) 
    {
        return u * a;
    }

    const Vector2d operator / (const double& a) const 
    {
        return Vector2d(x / a, y / a);
    }

    const Vector2d operator - () const 
    {
        return Vector2d(-x, -y);
    }

    Vector2d& operator += (const Vector2d& u) 
    {
        *this = *this + u;
        return *this;
    }

    Vector2d& operator -= (const Vector2d& u) 
    {
        *this = *this - u;
        return *this;
    }

    Vector2d& operator *= (const double& a) 
    {
        *this = *this * a;
        return *this;
    }

    Vector2d& operator /= (const double& a) 
    {
        *this = *this / a;
        return *this;
    }

    const double GetDistanceTo(const Vector2d& u) const 
    { 
        return sqrt(pow(u.x - x, 2) + pow(u.y - y, 2)); 
    }

    const double GetDeterminant(const Vector2d& vector2D) 
    {
        return vector2D.y * x - vector2D.x - y;
    }

    operator std::string() const 
    {
        std::ostringstream os;
        os << x << " " << y;
        return os.str();
    }

    virtual const Vector2d Accept(const ITransformationVisitor& transformationVisitor) const
    {
        return transformationVisitor.Visit(*this);
    }
};

inline extern std::ostream& operator << (std::ostream& os, const Vector2d& u) 
{
    os << (std::string)u;
    return os;
}