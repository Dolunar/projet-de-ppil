#pragma once
#include "ShapeParserChain.hpp"
#include "Circle.hpp"

using namespace std;

class CircleParser : public ChainShapeParser {
public:
    CircleParser(IShapeParser* nextParser = nullptr) : ChainShapeParser(nextParser) 
    {
    }

protected:
    virtual IShape* doParse(list<string>& lines) const 
    {
        double centerX, centerY, radius;
        unsigned long colorCode;
        string line;
        if (sscanf(lines.front().c_str(), "CIRCLE %lf %lf %lf %lx", &centerX, &centerY, &radius, &colorCode) != 4)
        {
            return nullptr;
        }
        lines.pop_front();
        return new Circle(Vector2d(centerX, centerY), radius, colorCode);
    }
};