#pragma once
#include <iostream>
#include "ITransformationVisitor.hpp"
#include "Vector2d.hpp"
#include "Circle.hpp"
#include "Line.hpp"
#include "Polygon.hpp"
#include "ShapeGroup.hpp"

using namespace std;

class TransformationUtils
{
public:
	static Circle* Visit(const ITransformationVisitor& tranformationVisitor, const Circle& circle) 
	{
		return new Circle(circle.GetCenter().Accept(tranformationVisitor), circle.GetRadius(), circle.GetColorCode());
	}

	static Line* Visit(const ITransformationVisitor& tranformationVisitor, const Line& line)
	{
		return new Line(line.GetPoint1().Accept(tranformationVisitor), line.GetPoint2().Accept(tranformationVisitor), line.GetColorCode());
	}

	static Polygone* Visit(const ITransformationVisitor& tranformationVisitor, const Polygone& polygon) {
		vector<Vector2d> transformedVertices;
		for (const Vector2d& vertex : polygon.GetVertices()) 
		{
			transformedVertices.push_back(vertex.Accept(tranformationVisitor));
		}
		return new Polygone(transformedVertices, polygon.GetColorCode());
	}

	static ShapeGroup* Visit(const ITransformationVisitor& tranformationVisitor, const ShapeGroup& shapeGroup)
	{
		vector<const IShape*> transformedShapes;
		for (const IShape* shape : shapeGroup.GetShapes()) 
		{
			transformedShapes.push_back(shape->Accept(tranformationVisitor));
		}
		return new ShapeGroup(transformedShapes);
	}
};