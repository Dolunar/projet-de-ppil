#pragma once
#include "ShapeParserChain.hpp"
#include "ShapeGroup.hpp"
#include "ParsingService.hpp"

using namespace std;

class ShapeGroupParser : public ChainShapeParser {
public:
    ShapeGroupParser(IShapeParser* nextParser = nullptr) : ChainShapeParser(nextParser) 
    {
    }

protected:
    virtual IShape* doParse(list<string>& lines) const {
        int shapesNumber;
        if (sscanf(lines.front().c_str(), "SHAPEGROUP %d", &shapesNumber) != 1)
        {
            return nullptr;
        }
        lines.pop_front();
        vector<const IShape*> shapes;
        string line;
        for (int i = 0; i < shapesNumber; i++) 
        {
            IShape* shape = ParsingService::parse(lines);
            if (shape == nullptr)
            {
                return nullptr;
            }
            shapes.push_back(shape);
        }
        return new ShapeGroup(shapes);
    }
};