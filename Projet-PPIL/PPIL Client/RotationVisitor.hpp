#pragma once
#include <iostream>
#include <cmath>
#include "ITransformationVisitor.hpp"
#include "TransformationUtils.hpp"
#include "Vector2d.hpp"
#include "Circle.hpp"
#include "Line.hpp"
#include "Polygon.hpp"

class RotationVisitor : public ITransformationVisitor
{
private:
	Vector2d rotationCenter;
	double cosOfAngleInRadians;
	double sinOfAngleInRadians;

public:
	RotationVisitor(const Vector2d& rotationCenter, const double& angleInRadians) {
		this->rotationCenter = rotationCenter;
		cosOfAngleInRadians = cos(angleInRadians);
		sinOfAngleInRadians = sin(angleInRadians);
	}

	virtual Vector2d Visit(const Vector2d& u) const 
	{
		Vector2d delta = Vector2d(u.x - rotationCenter.x, u.y - rotationCenter.y);
		return Vector2d(
			delta.x * cosOfAngleInRadians - delta.y * sinOfAngleInRadians + rotationCenter.x,
			delta.x * sinOfAngleInRadians + delta.y * cosOfAngleInRadians + rotationCenter.y
		);
	}

	virtual Circle* Visit(const Circle& circle) const
	{
		return TransformationUtils::Visit(*this, circle);
	}

	virtual Line* Visit(const Line& line) const
	{
		return TransformationUtils::Visit(*this, line);
	}

	virtual Polygone* Visit(const Polygone& polygon) const
	{
		return TransformationUtils::Visit(*this, polygon);
	}

	virtual ShapeGroup* Visit(const ShapeGroup& shapeGroup) const
	{
		return TransformationUtils::Visit(*this, shapeGroup);
	}
};