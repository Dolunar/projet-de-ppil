#pragma once
#include <iostream>
#include <fstream>
#include "IVisitor.hpp"
#include "Circle.hpp"
#include "Line.hpp"
#include "Polygon.hpp"
#include "ShapeGroup.hpp"

using namespace std;

class FileSaveVisitor : public IVisitor
{
private:
    ofstream file;

    void WriteInFile(const string& msg)
    {
        file << msg;
        file.close();
    }

public:
    FileSaveVisitor(string filename) 
    {
        file = ofstream(filename);
        if (!file.is_open()) 
        {
            throw runtime_error("FileSaveVisitor: Could not open file");
        }
    }

    void Visit(const Circle& circle) {
        WriteInFile(circle);
    }

    void Visit(const Line& line) {
        WriteInFile(line);
    }

    void Visit(const Polygone& polygon) {
        WriteInFile(polygon);
    }

    void Visit(const ShapeGroup& shapeGroup) {
        WriteInFile(shapeGroup);
    }
};

