#pragma once
#include "ShapeParserChain.hpp"
#include "Polygon.hpp"

class PolygonParser : public ChainShapeParser {
public:
    PolygonParser(IShapeParser* nextParser = nullptr) : ChainShapeParser(nextParser) 
    {
    }

protected:
    virtual IShape* doParse(list<string>& lines) const 
    {
        int verticesNumber;
        unsigned long colorCode;
        if (sscanf(lines.front().c_str(), "POLYGON %d %lx", &verticesNumber, &colorCode) != 2)
        {
            return nullptr;
        }
        lines.pop_front();
        vector<Vector2d> vertices;
        double x, y;
        for (int i = 0; i < verticesNumber; i++) 
        {
            sscanf(lines.front().c_str(), "%lf %lf", &x, &y);
            vertices.push_back(Vector2d(x, y));
            lines.pop_front();
        }
        return new Polygone(vertices, colorCode);
    }
};