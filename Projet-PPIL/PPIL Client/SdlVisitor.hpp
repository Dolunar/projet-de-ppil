#pragma once
#include <iostream>
#include <vector>
#include <SDL.h>
#include "IVisitor.hpp"
#include "IShape.hpp"
#include "Circle.hpp"
#include "Line.hpp"
#include "Polygon.hpp"

using namespace std;

class SdlVisitor : public IVisitor {
private:
    SDL_Renderer* renderer;

public:
    SdlVisitor(SDL_Renderer* renderer) {
        this->renderer = renderer;
    }

    void Visit(const Circle& circle) {
        Vector2d center = circle.GetCenter();
        double radius = circle.GetRadius();
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
        for (double dy = 1; dy <= circle.GetRadius(); dy += 1.0) {
            // Calcul de la largeur du segment � ce niveau de dy
            double dx = floor(sqrt((2.0 * radius * dy) - (dy * dy)));
            SDL_RenderDrawLine(renderer, center.x - dx, center.y + dy - radius, center.x + dx, center.y + dy - radius);
            SDL_RenderDrawLine(renderer, center.x - dx, center.y - dy + radius, center.x + dx, center.y - dy + radius);
        }
        SDL_RenderPresent(renderer);
    }

    void Visit(const Line& line) {
        Vector2d start = line.GetPoint1();
        Vector2d end = line.GetPoint2();
        SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
        SDL_RenderDrawLine(renderer, start.x, start.y, end.x, end.y);
        SDL_RenderPresent(renderer);
    }

    void Visit(const Polygone& polygon) {
        SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
        vector<Vector2d> vertices = polygon.GetVertices();
        for (int i = 0; i < vertices.size() - 1; i++) {
            Vector2d start = vertices[i];
            Vector2d end = vertices[i + 1];
            SDL_RenderDrawLine(renderer, start.x, start.y, end.x, end.y);
        }
        Vector2d start = vertices.front();
        Vector2d end = vertices.back();

        SDL_RenderDrawLine(renderer, start.x, start.y, end.x, end.y);
        SDL_RenderPresent(renderer);
    }

    void Visit(const ShapeGroup& shapeGroup) {
        for (const IShape* shape : shapeGroup.GetShapes()) {
            shape->Accept(*this);
        }
    }
};