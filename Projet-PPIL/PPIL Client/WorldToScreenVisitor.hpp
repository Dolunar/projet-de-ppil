#pragma once
#include <iostream>
#include "ITransformationVisitor.hpp"
#include "TransformationUtils.hpp"
#include "Vector2d.hpp"
#include "Circle.hpp"
#include "Line.hpp"
#include "Polygon.hpp"

class WorldToScreenVisitor : public ITransformationVisitor {
private:
    double lambda;
    double a;
    double b;
    Vector2d epsilon;

    const Vector2d Convert(const Vector2d& u) const 
    {
        return Vector2d(lambda * epsilon.x * u.x + a, lambda * epsilon.y * u.y + b);
    }

    const double Convert(const double& distance) const 
    {
        return lambda * distance;
    }

public:
    WorldToScreenVisitor(const Vector2d& worldX, const Vector2d& worldY, const Vector2d& screenX, const Vector2d& screenY) {
        lambda = std::min(abs(screenY.x - screenX.x) / abs(worldY.x - worldX.x), abs(screenY.y - screenX.y) / abs(worldY.y - worldX.y));

        epsilon.x = (signbit(worldY.x - worldX.x) == signbit(screenY.x - screenX.x)) ? 1 : -1;
        epsilon.y = (signbit(worldY.y - worldX.y) == signbit(screenY.y - screenX.y)) ? 1 : -1;

        Vector2d c1((worldX.x + worldY.x) / 2, (worldX.y + worldY.y) / 2);
        Vector2d c2((screenX.x + screenY.x) / 2, (screenX.y + screenY.y) / 2);

        a = c2.x - lambda * epsilon.x * c1.x;
        b = c2.y - lambda * epsilon.y * c1.y;
    }

    virtual Vector2d Visit(const Vector2d& u) const
    {
        return Convert(u);
    }

    virtual Circle* Visit(const Circle& circle) const
    {
        return new Circle(Convert(circle.GetCenter()), Convert(circle.GetRadius()), circle.GetColorCode());
    }

    virtual Line* Visit(const Line& line) const
    {
        return TransformationUtils::Visit(*this, line);
    }

    virtual Polygone* Visit(const Polygone& polygon) const
    {
        return TransformationUtils::Visit(*this, polygon);
    }

    virtual ShapeGroup* Visit(const ShapeGroup& shapeGroup) const
    {
        return TransformationUtils::Visit(*this, shapeGroup);
    }
};