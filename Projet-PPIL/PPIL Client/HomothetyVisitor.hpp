#pragma once
#include <iostream>
#include "ITransformationVisitor.hpp"
#include "TransformationUtils.hpp"
#include "Vector2d.hpp"
#include "Circle.hpp"
#include "Line.hpp"
#include "Polygon.hpp"

class HomothetyVisitor : public ITransformationVisitor
{
private:
	Vector2d homothetyCenter;
	double scale;

public:
	HomothetyVisitor(const Vector2d& homothetyCenter, const double& scale) 
	{
		if (scale <= 0) 
		{
			throw "Scale must be > 0";
		}
		this->homothetyCenter = homothetyCenter;
		this->scale = scale;
	}

	virtual Vector2d Visit(const Vector2d& u) const 
	{
		return scale * u + homothetyCenter * (1 - scale);
	}

	virtual Circle* Visit(const Circle& circle) const
	{
		return new Circle(circle.GetCenter().Accept(*this), circle.GetRadius() * scale, circle.GetColorCode());
	}

	virtual Line* Visit(const Line& line) const
	{
		return TransformationUtils::Visit(*this, line);
	}

	virtual Polygone* Visit(const Polygone& polygon) const
	{
		return TransformationUtils::Visit(*this, polygon);
	}

	virtual ShapeGroup* Visit(const ShapeGroup& shapeGroup) const
	{
		return TransformationUtils::Visit(*this, shapeGroup);
	}
};