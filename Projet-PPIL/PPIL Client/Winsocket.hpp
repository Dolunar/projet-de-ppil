#pragma once
#pragma warning(disable:4996)
#include <iostream>
#include <sstream>
#include <string>
#include <string.h>
#include <winsock2.h>
#include "ISocket.hpp"
#pragma comment(lib, "ws2_32.lib")

using namespace std;

class Winsocket : public ISocket
{
private:
	static bool isWinsockLibraryInitialized;

	const char* address;
	int port;
	SOCKET mySocket;

public:
	Winsocket(const char* address, const int& port)
	{
		this->address = address;
		this->port = port;

		if (!Winsocket::isWinsockLibraryInitialized)
		{
			Winsocket::InitializeWinsockLibrary();
			Winsocket::isWinsockLibraryInitialized = true;
		}

		mySocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		if (mySocket == INVALID_SOCKET)
		{
			throw runtime_error("Socket creation failed: " + to_string(WSAGetLastError()));
		}
	}

	virtual void Open()
	{
		SOCKADDR_IN sockaddr;
		sockaddr.sin_family = AF_INET;
		sockaddr.sin_addr.s_addr = inet_addr(address);
		sockaddr.sin_port = htons(port);

		if (connect(mySocket, (SOCKADDR*)&sockaddr, sizeof(sockaddr)) == SOCKET_ERROR)
		{
			throw runtime_error("Server connection failed");
		}
	}

	virtual void Send(string msg)
	{
		string formattedMsg = msg + "\r\n";

		if (send(mySocket, formattedMsg.c_str(), formattedMsg.size(), 0) == SOCKET_ERROR)
		{
			throw runtime_error("Message sending failed");
		}
	}

	virtual void Close()
	{
		if (mySocket != INVALID_SOCKET) 
		{
			shutdown(mySocket, SD_SEND);
			closesocket(mySocket);
			mySocket = INVALID_SOCKET;
		}
	}

private:
	static void InitializeWinsockLibrary()
	{
		WSADATA wsaData;

		if (WSAStartup(MAKEWORD(2, 0), &wsaData))
		{
			throw runtime_error("Winsock initialization failed");
		}

		cout << "Winsock library initalized!";
	}
};