#pragma once
#include <iostream>
#include "IShape.hpp"

using namespace std;

static const double LINE_AREA_CONVENTION = 0.0;

class Line : public IShape
{
private:
	Vector2d point1, point2;
	unsigned int colorCode;

public:
	Line(const Vector2d& point1, const Vector2d& point2, const unsigned int& colorCode)
	{
		this->point1 = point1;
		this->point2 = point2;
		this->colorCode = colorCode;
	}

	const Vector2d& GetPoint1() const
	{
		return point1;
	}

	const Vector2d& GetPoint2() const
	{
		return point2;
	}

	unsigned int GetColorCode() const
	{
		return colorCode;
	}

	virtual double GetArea() const 
	{
		return LINE_AREA_CONVENTION;
	}

    virtual operator string() const 
	{
		std::ostringstream os;
		os << "LINE " << point1 << " " << point2 << " " << hex << colorCode << std::endl;
		return os.str();
	}

	virtual Line* Clone() const
	{
		return new Line(point1, point2, colorCode);
	}

	virtual void Accept(IVisitor& visitor) const
	{
		visitor.Visit(*this);
	}

	virtual Line* Accept(const ITransformationVisitor& transformationVisitor) const
	{
		return transformationVisitor.Visit(*this);
	}
};
