#pragma once
#include <iostream>
#include <vector>
#include <initializer_list>
#include <stdexcept>
#include <sstream>
#include "IShape.hpp"

using std::vector;
using std::invalid_argument;
using std::ostringstream;

class ShapeGroup : public IShape {
private:
    vector<const IShape*> shapes;

    void ValidateShape(const IShape* shape) {
        if (shape == nullptr) {
            throw invalid_argument("Shape must not be null");
        }
    }

public:
    // Constructor using initializer_list for better initialization
    ShapeGroup(const initializer_list<const IShape*>& initList)
    {
        if (initList.size() <= 0) {
            throw invalid_argument("ShapeGroup cannot be empty");
        }
        for (const auto& shape : initList) {
            ValidateShape(shape);
            shapes.push_back(shape);
        }
    }

    // Constructor using vector
    ShapeGroup(const vector<const IShape*>& shapes)
        : shapes(shapes) {
        if (shapes.empty()) {
            throw invalid_argument("ShapeGroup cannot be empty");
        }
        for (const auto& shape : shapes) {
            ValidateShape(shape);
        }
    }

    const vector<const IShape*>& GetShapes() const {
        return shapes;
    }

    virtual double GetArea() const override {
        double totalArea = 0;
        for (const IShape* shape : shapes) {
            totalArea += shape->GetArea();
        }
        return totalArea;
    }

    virtual operator string() const override {
        ostringstream os;
        os << "SHAPEGROUP " << shapes.size() << "\n";
        for (const auto& shape : shapes) {
            os << static_cast<string>(*shape);
        }
        return os.str();
    }

    virtual ShapeGroup* Clone() const override {
        vector<const IShape*> clonedShapes;
        for (const auto& shape : shapes) {
            clonedShapes.push_back(shape->Clone());
        }
        return new ShapeGroup(clonedShapes);
    }

    virtual void Accept(IVisitor& visitor) const override {
        visitor.Visit(*this);
    }

    virtual ShapeGroup* Accept(const ITransformationVisitor& transformationVisitor) const override {
        return transformationVisitor.Visit(*this);
    }
};