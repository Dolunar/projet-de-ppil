#pragma once
#include "ParsingService.hpp"
#include "CircleParser.hpp"
#include "LineParser.hpp"
#include "PolygonParser.hpp"
#include "ShapeGroupParser.hpp"

class ParserInitializerFacade
{
public:
	static void Init()
	{
		ParsingService::shapeParser = new CircleParser(new LineParser(new PolygonParser(new ShapeGroupParser())));
	}
};