#pragma once
#include <iostream>

class Circle;
class Line;
class Polygone;
class ShapeGroup;

class IVisitor 
{
public:
	virtual void Visit(const Circle& circle) = 0;
	virtual void Visit(const Line& line) = 0;
	virtual void Visit(const Polygone& polygon) = 0;
	virtual void Visit(const ShapeGroup& shapeGroup) = 0;
};