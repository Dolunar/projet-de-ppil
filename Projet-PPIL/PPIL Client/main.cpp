#pragma warning(disable:4996)
#include <iostream>
#include "Vector2d.hpp"
#include "Circle.hpp"
#include "Line.hpp"
#include "Polygon.hpp"
#include "FileSaveVisitor.hpp"
#include "ParsingService.hpp"
#include "ParserInitializerFacade.hpp"

#include "Winsocket.hpp"
#include "ServerDrawVisitor.hpp"
#include "SdlVisitor.hpp"

#include "TranslationVisitor.hpp"
#include "HomothetyVisitor.hpp"
#include "RotationVisitor.hpp"

#include <SDL.h>

using namespace std;
int main(int argc, char* argv[]) {
    auto circle = new Circle(Vector2d(200, 300), 50, 0xff0000ff);
    auto rectangle = new Line(Vector2d(400, 400), Vector2d(500, 400), 0x0000ffff);

    auto transformer = new TranslationVisitor(Vector2d(0, -200));

    circle = circle->Accept(*transformer);
    rectangle = rectangle->Accept(*transformer);

    auto group = new ShapeGroup({ circle, rectangle });

    SDL_Window* window = nullptr;
    SDL_Renderer* renderer = nullptr;

    // Initialisation de SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initialize! SDL_Error: " << SDL_GetError() << std::endl;
        return -1;
    }

    // Cr�ation de la fen�tre
    window = SDL_CreateWindow("SDL Circle", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
    if (window == nullptr) {
        std::cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        return -1;
    }

    // Cr�ation du renderer
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr) {
        std::cerr << "Renderer could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        SDL_DestroyWindow(window);
        SDL_Quit();
        return -1;
    }

    // Couleur de fond
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Blanc
    SDL_RenderClear(renderer);

    SdlVisitor visitor(renderer);
    group->Accept(visitor);
    
    auto txtVisitor = FileSaveVisitor("C:\\Users\\Vincent\\Desktop\\Projet-PPIL\\PPIL Client\\fichier.txt");
    group->Accept(txtVisitor);

    // Boucle principale
    bool running = true;
    SDL_Event e;
    while (running) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                running = false;
            }
        }
    }

    // Nettoyage
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}

/*
int main(int argc, char* argv[])
{
    try
    {
        IShape* shape = new Polygone({ Vector2d(0, 0), Vector2d(100, 0), Vector2d(0, 100) }, 0xff0000);

        cout << *shape;


        IShape* shape1 = new Polygone({ Vector2d(0, 0), Vector2d(200, 0), Vector2d(200, 200), Vector2d(0, 200) }, 0x0000ffff);

        ITransformationVisitor* visitor = new HomothetyVisitor(Vector2d(1000, 500), 0.5);
        IShape* shape2 = shape1->Accept(*visitor);
        cout << *shape2;

        ISocket* socket = new Winsocket("26.105.58.207", 2555);
        socket->Open();
        IVisitor* serverVisitor = new ServerDrawVisitor(socket);

        shape2->Accept(*serverVisitor);
        socket->Close();



    }
    catch (const exception& e)
    {
        cout << "Erreur:" << e.what() << endl;
    }


    return EXIT_SUCCESS;
}
*/