#pragma once
#include <iostream>
#include "Vector2d.hpp"
#include "IVisitor.hpp"
#include "ITransformationVisitor.hpp"

using namespace std;

class IShape
{
public:
	virtual double GetArea() const = 0;
	virtual operator string() const = 0;
	virtual IShape* Clone() const = 0;
	virtual void Accept(IVisitor& visitor) const = 0;
	virtual IShape* Accept(const ITransformationVisitor& transformationVisitor) const = 0;
};

inline ostream& operator << (ostream& os, const IShape& shape) {
	os << (string)shape;
	return os;
}