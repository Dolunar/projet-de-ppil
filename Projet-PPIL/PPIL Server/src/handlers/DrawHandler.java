package handlers;

import draw.DrawAPI;

import java.util.LinkedList;

public interface DrawHandler {
    boolean handle(DrawAPI drawAPI, LinkedList<String> request);
}
