package handlers;

import draw.DrawAPI;

import javax.vecmath.Vector2d;
import java.util.LinkedList;

public class CircleHandler extends DrawHandlerChain {
    public CircleHandler(DrawHandler next) {
        super(next);
    }

    @Override
    public boolean doHandle(DrawAPI drawAPI, LinkedList<String> request) {
        if (!HandlerUtils.isHandable(request, "CIRCLE")) {
            return false;
        }

        String[] splitLine = HandlerUtils.splitAndRemoveFirst(request);

        Vector2d center = HandlerUtils.parseVector2d(splitLine[1], splitLine[2]);
        double radius = Double.parseDouble(splitLine[3]);
        long colorCode = HandlerUtils.parseColorCode(splitLine[4]);

        drawAPI.drawCircle(center, radius, colorCode);

        return true;
    }
}
