package handlers;

import javax.vecmath.Vector2d;
import java.util.LinkedList;

public class HandlerUtils {
    private HandlerUtils() {
    }

    public static boolean isHandable(LinkedList<String> request, String keyword) {
        String head = request.getFirst();
        System.out.println(head);
        return head.contains(keyword);
    }

    public static String[] splitAndRemoveFirst(LinkedList<String> request) {
        String head = request.removeFirst();
        return head.split(" ");
    }

    public static Vector2d parseVector2d(String x, String y) {
        return new Vector2d(
                Double.parseDouble(x),
                Double.parseDouble(y)
        );
    }

    public static long parseColorCode(String colorCodeString) {
        return Long.parseLong(colorCodeString, 16);
    }
}
