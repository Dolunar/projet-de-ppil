package handlers;



import draw.DrawAPI;

import javax.vecmath.Vector2d;
import java.util.LinkedList;

public class PolygonHandler extends DrawHandlerChain {
    public PolygonHandler(DrawHandler next) {
        super(next);
    }

    @Override
    public boolean doHandle(DrawAPI drawAPI, LinkedList<String> request) {
        if (!HandlerUtils.isHandable(request, "POLYGON")) {
            return false;
        }

        String[] splitLine = HandlerUtils.splitAndRemoveFirst(request);

        int verticesNumber = Integer.parseInt(splitLine[1]);
        long colorCode = HandlerUtils.parseColorCode(splitLine[2]);

        LinkedList<Vector2d> vertices = new LinkedList<>();

        for (int i = 0; i < verticesNumber; i++) {
            String[] vertex = HandlerUtils.splitAndRemoveFirst(request);
            vertices.add(HandlerUtils.parseVector2d(vertex[0], vertex[1]));
        }

        drawAPI.drawPolygon(vertices, colorCode);

        return true;
    }
}
