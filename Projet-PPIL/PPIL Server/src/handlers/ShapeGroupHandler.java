package handlers;

import draw.DrawAPI;
import draw.DrawService;

import java.util.LinkedList;

public class ShapeGroupHandler extends DrawHandlerChain {
    private final DrawService drawService = DrawService.getInstance();

    public ShapeGroupHandler(DrawHandler next) {
        super(next);
    }

    @Override
    public boolean doHandle(DrawAPI drawAPI, LinkedList<String> request) {
        if (!HandlerUtils.isHandable(request, "SHAPEGROUP")) {
            return false;
        }

        String[] splitLine = HandlerUtils.splitAndRemoveFirst(request);
        int shapeCount = Integer.parseInt(splitLine[1]);

        for (int i = 0; i < shapeCount; i++) {
            drawService.draw(drawAPI, request);
        }

        return true;
    }
}
