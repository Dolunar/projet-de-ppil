package handlers;

import draw.DrawAPI;

import java.util.LinkedList;

public abstract class DrawHandlerChain implements DrawHandler {
    private final DrawHandler next;

    public DrawHandlerChain(DrawHandler next) {
        this.next = next;
    }

    @Override
    public final boolean handle(DrawAPI drawAPI, LinkedList<String> request) {
        if (request.isEmpty()) {
            throw new IllegalArgumentException("request is empty");
        }

        if (doHandle(drawAPI, request)) {
            return true;
        }

        if (next != null) {
            return next.handle(drawAPI, request);
        }

        return false;
    }

    public abstract boolean doHandle(DrawAPI drawAPI, LinkedList<String> request);
}
