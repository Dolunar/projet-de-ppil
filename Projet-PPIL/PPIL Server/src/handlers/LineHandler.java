package handlers;

import draw.DrawAPI;

import javax.vecmath.Vector2d;
import java.util.LinkedList;

public class LineHandler extends DrawHandlerChain {
    public LineHandler(DrawHandler next) {
        super(next);
    }

    @Override
    public boolean doHandle(DrawAPI drawAPI, LinkedList<String> request) {
        if (!HandlerUtils.isHandable(request, "LINE")) {
            return false;
        }

        String[] splitHead = HandlerUtils.splitAndRemoveFirst(request);

        Vector2d point1 = HandlerUtils.parseVector2d(splitHead[1], splitHead[2]);
        Vector2d point2 = HandlerUtils.parseVector2d(splitHead[3], splitHead[4]);
        long colorCode = HandlerUtils.parseColorCode(splitHead[5]);

        drawAPI.drawLine(point1, point2, colorCode);

        return true;
    }
}

