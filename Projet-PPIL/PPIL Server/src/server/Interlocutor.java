package server;

import draw.DrawAPI;
import draw.DrawService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.Buffer;
import java.util.LinkedList;

public class Interlocutor extends Thread {
    public static final int CLIENT_RECEPTION_DELAY_IN_MS = 5;
    public static String clientEndMessage = "END";

    private final DrawService drawService = DrawService.getInstance();
    private final Socket clientSocket;
    private final BufferedReader clientReader;
    private final DrawAPI drawAPI;

    public Interlocutor(Socket clientSocket  , DrawAPI drawAPI, ThreadGroup threadGroup) throws IOException {
        super(threadGroup, "Client request receiver");
        this.clientSocket = clientSocket;
        this.clientReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        this.drawAPI = drawAPI;
    }

    public LinkedList<String> receiveRequest(BufferedReader inputReader) throws IOException {
        LinkedList<String> requestLines  = new LinkedList<>();
        String line = inputReader.readLine();
        do {
            requestLines.add(line);
            line = inputReader.readLine();
        } while (!line.equals(clientEndMessage));
        return requestLines;
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                LinkedList<String> request = receiveRequest(clientReader);
                DrawService.getInstance().draw(drawAPI, request);
                Thread.sleep(CLIENT_RECEPTION_DELAY_IN_MS);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Client disconnected");
        }
    }
}
