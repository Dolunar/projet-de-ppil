package server;

import draw.DrawAPI;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class DrawServer extends Thread {
    private final ServerSocket serverSocket;
    private final DrawAPI drawAPI;
    private final ThreadGroup threadGroup = new ThreadGroup("Client request receivers");
    private int clientCount = 0;

    public DrawServer(ServerSocket serverSocket, DrawAPI drawAPI) {
        this.serverSocket = serverSocket;
        this.drawAPI = drawAPI;
    }

    public String getAddress() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostAddress();
    }

    int getPort() {
        return serverSocket.getLocalPort();
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                Socket clientSocket = serverSocket.accept();
                clientCount++;
                System.out.println("Client number " + clientCount + " connected");
                Interlocutor interlocutor = new Interlocutor(clientSocket, drawAPI, threadGroup);
                interlocutor.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
