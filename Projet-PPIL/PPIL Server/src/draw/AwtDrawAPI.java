package draw;

import javax.vecmath.Vector2d;
import java.awt.*;
import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.List;

public class AwtDrawAPI implements DrawAPI {
    private AwtDrawWindow window;
    private Graphics graphics;

    private AwtDrawAPI() {}

    public AwtDrawAPI(AwtDrawWindow window) {
        if (window == null) {
            throw new IllegalArgumentException("Window must not be null");
        }
        this.window = window;
    }

    private Color convertColorCode(long colorCode) {
        float redRatio = (float) ((colorCode >> 24) & 0xFF) / 255;
        float greenRatio = (float) ((colorCode >> 16) & 0xFF) / 255;
        float blueRatio = (float) ((colorCode >> 8) & 0xFF) / 255;
        float alphaRatio = (float) ((colorCode) & 0xFF) / 255;
        return new Color(redRatio, greenRatio, blueRatio, alphaRatio);
    }

    @Override
    public void drawCircle(Vector2d center, double radius, long colorCode) {
        System.out.println("drawCircle");

        Color color = convertColorCode(colorCode);
        window.getGraphics().setColor(color);

        int leftX = (int) (center.x - radius);
        int topY = (int) (center.y - radius);
        int diameter = (int) (radius * 2);
        window.getGraphics().drawOval(leftX, topY, diameter, diameter);

        window.getBufferStrategy().show();
    }

    @Override
    public void drawLine(Vector2d point1, Vector2d point2, long colorCode) {
        System.out.println("drawLine");

        Color color = convertColorCode(colorCode);
        window.getGraphics().setColor(color);

        int x1 = (int) point1.x;
        int y1 = (int) point1.y;
        int x2 = (int) point2.x;
        int y2 = (int) point2.y;
        window.getGraphics().drawLine(x1, y1, x2, y2);

        window.getBufferStrategy().show();
    }

    @Override
    public void drawPolygon(List<Vector2d> vertices, long colorCode) {
        System.out.println("drawPolygon");

        Color color = convertColorCode(colorCode);
        window.getGraphics().setColor(color);

        int[] xPoints = vertices.stream()
                .mapToInt(v -> (int) v.x)
                .toArray();

        int[] yPoints = vertices.stream()
                .mapToInt(v -> (int) v.y)
                .toArray();

        System.err.println("xPoints: " + Arrays.toString(xPoints));
        System.err.println("yPoints: " + Arrays.toString(yPoints));

        window.getGraphics().drawPolygon(xPoints, yPoints, vertices.size());

        window.getBufferStrategy().show();
    }
}