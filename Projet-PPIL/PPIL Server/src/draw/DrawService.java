package draw;

import handlers.DrawHandler;

import java.util.LinkedList;

public class DrawService {
    private static volatile DrawService instance = null;

    private DrawHandler handler;

    public static DrawService getInstance() {
        if (instance == null) {
            synchronized (DrawService.class) {
                if (instance == null) {
                    instance = new DrawService();
                }
            }
        }
        return instance;
    }

    private DrawService() {
    }

    public void setHandler(DrawHandler handler) {
        this.handler = handler;
    }

    public void draw(DrawAPI drawAPI, LinkedList<String> request) {
        if (handler == null) {
            throw new IllegalStateException("handler is not set");
        }

        if (!handler.handle(drawAPI, request)) {
            throw new IllegalArgumentException("request is not handable");
        }
    }
}
