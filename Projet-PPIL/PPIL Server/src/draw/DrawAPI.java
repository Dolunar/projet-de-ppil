package draw;

import javax.vecmath.Vector2d;
import java.util.List;

public interface DrawAPI {
    void drawCircle(Vector2d center, double radius, long colorCode);
    void drawLine(Vector2d point1, Vector2d point2, long colorCode);
    void drawPolygon(List<Vector2d> vertices, long colorCode);
}