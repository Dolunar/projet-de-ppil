package draw;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;

public class AwtDrawWindow extends Frame {
    private static final int BUFFER_NUMBER = 2;
    private static final long BUFFER_STRATEGY_INIT_DELAY_IN_MS = 2000;

    public static Color defaultBackgroundColor = Color.WHITE;
    public static int upperLeftCornerX = 0;
    public static int upperLeftCornerY = 0;

    private JFrame _frame;
    private BufferStrategy _bufferStrategy;
    private Graphics _graphics;
    private boolean _isOpen = false;

    public AwtDrawWindow(String title) {
        super(title);
    }

    public BufferStrategy getBufferStrategy() {
        return _bufferStrategy;
    }

    public Graphics getGraphics() {
        return _graphics;
    }

    public void open(int width, int height) throws InterruptedException {
        _frame = new JFrame(getTitle());
        _frame.setBounds(upperLeftCornerX, upperLeftCornerY, width, height);
        _frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        _frame.setResizable(false);
        _frame.setBackground(defaultBackgroundColor);
        _frame.setVisible(true);
        _frame.createBufferStrategy(BUFFER_NUMBER);
        waitForBufferStrategyInit();
        _bufferStrategy = _frame.getBufferStrategy();
        _graphics = _frame.getBufferStrategy().getDrawGraphics();
        _frame.setIgnoreRepaint(true);
        _isOpen = true;
    }

    public boolean isOpen() {
        return _isOpen;
    }

    public void close() {
        _frame.dispose();
        _isOpen = false;
    }

    private void waitForBufferStrategyInit() throws InterruptedException {
        Thread.sleep(BUFFER_STRATEGY_INIT_DELAY_IN_MS);
    }


}