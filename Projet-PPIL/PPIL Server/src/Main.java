import draw.AwtDrawAPI;
import draw.AwtDrawWindow;
import draw.DrawAPI;
import draw.DrawService;
import handlers.CircleHandler;
import handlers.LineHandler;
import handlers.PolygonHandler;
import handlers.ShapeGroupHandler;
import server.DrawServer;

import java.net.ServerSocket;

public class Main {
    public static void main(String[] args) throws Exception {
        DrawService drawService = DrawService.getInstance();
        drawService.setHandler(
                new CircleHandler(
                        new LineHandler(
                                new PolygonHandler(
                                        new ShapeGroupHandler(null)
                                )
                        )
                )
        );

        AwtDrawWindow drawWindow = new AwtDrawWindow("Draw Window");
        DrawAPI drawAPI = new AwtDrawAPI(drawWindow);
        DrawServer drawServer = new DrawServer(new ServerSocket(2555), drawAPI);
        System.out.println(drawServer.getAddress());
        drawWindow.open(2000, 1000);
        drawServer.start();
    }
}